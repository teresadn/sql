﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SQL
{
    class Vendor
    {
        public string id;
        public string name;
        public string surname;
        public string state;
        public string phone;
        public DateTime date;
        public int size;

        private string connectionString;
        private SqlConnection sqlConnection;

        public Vendor()
        {
            Init();
        }
        public Vendor(string vendor_id)
        {
            Init();
            Load(vendor_id);
        }
        private bool Init()
        {
            connectionString = "Server=LAPTOP-JIHJSJKB\\SQLEXPRESS;Database=Test;Trusted_Connection=True;";
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            return true;
        }

        public bool Load(string vendor_id)
        {
            string query = "select * from VENDORS where VENDOR_ID='" + vendor_id + "'";
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                id = Convert.ToString(sqlDataReader["VENDOR_ID"]);
                name = Convert.ToString(sqlDataReader["VENDOR_NAME"]);
                surname = Convert.ToString(sqlDataReader["VENDOR_SURNAME"]);
                state = Convert.ToString(sqlDataReader["VENDOR_STATE"]);
                phone = Convert.ToString(sqlDataReader["VENDOR_PHONE"]);
                date = Convert.ToDateTime(sqlDataReader["VENDOR_DATE"]);
                size = Convert.ToInt32(sqlDataReader["VENDOR_SIZE"]);
            }

            sqlDataReader.Close();

            return true;
        }

        public bool Insert()
        {
            string query = string.Format("insert into VENDORS values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
    , id, name, surname,
    state, phone, date, size);
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.ExecuteNonQuery();

            return true;
        }

        public bool update()
        {
            try
            {
                string query = string.Format("update VENDORS set VENDOR_NAME='{1}',VENDOR_SURNAME='{2}',"
        + "VENDOR_STATE='{3}',VENDOR_PHONE='{4}',VENDOR_DATE='{5}',VENDOR_SIZE='{6}'" +
        " where VENDOR_ID='{0}'", id, name, surname,
        state, phone, date, size);
                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
