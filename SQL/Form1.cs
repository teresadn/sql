﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SQL
{
    public partial class sqlForm : Form
    {
        string connectionString;
        SqlConnection sqlConnection;

        public sqlForm()
        {
            InitializeComponent();
            connectionString = "Server=LAPTOP-JIHJSJKB\\SQLEXPRESS;Database=Test;Trusted_Connection=True;";
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
        }
        private void refreshList()
        {
            vendorIDListBox.Items.Clear();  //elimina il vecchio nella lista

            string query2 = "select * from VENDORS";
            SqlCommand sqlCommand2 = new SqlCommand(query2, sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand2.ExecuteReader();
            while (sqlDataReader.Read())
            {
                string vendorID = sqlDataReader["VENDOR_ID"].ToString();
                vendorIDListBox.Items.Add(vendorID);
            }

            sqlDataReader.Close();
        }

        private void sqlForm_Load(object sender, EventArgs e)
        {
            refreshList();
        }

        private void vendorIDListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSelectVendor();
        }

        private void LoadSelectVendor()
        {
            if (vendorIDListBox.SelectedItem != null)
            {
                string vendor_id = vendorIDListBox.SelectedItem.ToString();

                Vendor vendor = new Vendor();
                bool ok = vendor.Load(vendor_id);

                if (ok)
                {
                    idTextBox.Text = vendor.id;
                    firstNameTextBox.Text = vendor.name;
                    lastNameTextBox.Text = vendor.surname;
                    stateTextBox.Text = vendor.state;
                    phoneTextBox.Text = vendor.phone;
                    dateTextBox.Text = vendor.date.ToString();
                    sizeTextBox.Text = vendor.size.ToString();
                }
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            string query = string.Format("delete from VENDORS where VENDOR_ID='{0}'", idTextBox.Text);
            SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.ExecuteNonQuery();

            refreshList();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            refreshList();
            //show bello
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Vendor vendor = new Vendor();
            vendor.id = idTextBox.Text;
            vendor.name = firstNameTextBox.Text;
            vendor.surname = lastNameTextBox.Text;
            vendor.state = stateTextBox.Text;
            vendor.phone = phoneTextBox.Text;
            vendor.date = Convert.ToDateTime(dateTextBox.Text);
            vendor.size = Convert.ToInt32(sizeTextBox.Text);

            if (vendor.surname.Length > 10)
                MessageBox.Show("Surname must be less than 10 characters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {//Data is good
                if (editButton.Text == "Edit")
                {
                    bool ok = vendor.update();
                }
                else
                {
                    bool ok = vendor.Insert();
                }

                refreshList();
            }
        }

        private void idTextBox_TextChanged(object sender, EventArgs e)
        {
            editButton.Text = vendorIDListBox.Items.Contains(idTextBox.Text) ? "Edit" : "Insert";
        }
    }
}
